#!/usr/bin/python
# TSL2561 I2C Light-To-Digital converter library for the Raspberry Pi.
# Datasheet: https://www.adafruit.com/datasheets/TSL2561.pdf
#
# based on Cedric Maion (https://github.com/cmaion/TSL2561.git)

import time
import logging
import smbus
#from Adafruit_I2C import Adafruit_I2C
import RPi.GPIO as GPIO
import Adafruit_GPIO.I2C as I2C
i2c = I2C
device=i2c.get_i2c_device(0x77) # address of BMP
from smbus import SMBus

#register  
TSL2561_REG_Control     = 0x80
TSL2561_REG_Timing      = 0x81
TSL2561_REG_Interrupt   = 0x86
TSL2561_REG_Channel0L   = 0x8C
TSL2561_REG_Channel0H   = 0x8D
TSL2561_REG_Channel1L   = 0x8E
TSL2561_REG_Channel1H   = 0x8F

# I2C Address
TSL2561_Address         = 0x29 #device address

#Parameters 
TSL2561_PARAM_LUX_SCALE     = 14 # scale by 2^14
TSL2561_PARAM_RATIO_SCALE   = 9 # scale ratio by 2^9
TSL2561_PARAM_CH_SCALE      = 10 # scale channel values by 2^10
TSL2561_PARAM_CHSCALE_TINT0 = 0x7517 # 322/11 * 2^CH_SCALE
TSL2561_PARAM_CHSCALE_TINT1 = 0x0fe7 # 322/81 * 2^CH_SCALE

TSL2561_PARAM_K1T = 0x0040 # 0.125 * 2^RATIO_SCALE
TSL2561_PARAM_B1T = 0x01f2 # 0.0304 * 2^LUX_SCALE
TSL2561_PARAM_M1T = 0x01be # 0.0272 * 2^LUX_SCALE
TSL2561_PARAM_K2T = 0x0080 # 0.250 * 2^RATIO_SCA
TSL2561_PARAM_B2T = 0x0214 # 0.0325 * 2^LUX_SCALE
TSL2561_PARAM_M2T = 0x02d1 # 0.0440 * 2^LUX_SCALE
TSL2561_PARAM_K3T = 0x00c0 # 0.375 * 2^RATIO_SCALE
TSL2561_PARAM_B3T = 0x023f # 0.0351 * 2^LUX_SCALE
TSL2561_PARAM_M3T = 0x037b # 0.0544 * 2^LUX_SCALE
TSL2561_PARAM_K4T = 0x0100 # 0.50 * 2^RATIO_SCALE
TSL2561_PARAM_B4T = 0x0270 # 0.0381 * 2^LUX_SCALE
TSL2561_PARAM_M4T = 0x03fe # 0.0624 * 2^LUX_SCALE
TSL2561_PARAM_K5T = 0x0138 # 0.61 * 2^RATIO_SCALE
TSL2561_PARAM_B5T = 0x016f # 0.0224 * 2^LUX_SCALE
TSL2561_PARAM_M5T = 0x01fc # 0.0310 * 2^LUX_SCALE
TSL2561_PARAM_K6T = 0x019a # 0.80 * 2^RATIO_SCALE
TSL2561_PARAM_B6T = 0x00d2 # 0.0128 * 2^LUX_SCALE
TSL2561_PARAM_M6T = 0x00fb # 0.0153 * 2^LUX_SCALE
TSL2561_PARAM_K7T = 0x029a # 1.3 * 2^RATIO_SCALE
TSL2561_PARAM_B7T = 0x0018 # 0.00146 * 2^LUX_SCALE
TSL2561_PARAM_M7T = 0x0012 # 0.00112 * 2^LUX_SCALE
TSL2561_PARAM_K8T = 0x029a # 1.3 * 2^RATIO_SCALE
TSL2561_PARAM_B8T = 0x0000 # 0.000 * 2^LUX_SCALE
TSL2561_PARAM_M8T = 0x0000 # 0.000 * 2^LUX_SCALE

TSL2561_PARAM_K1C = 0x0043 # 0.130 * 2^RATIO_SCALE
TSL2561_PARAM_B1C = 0x0204 # 0.0315 * 2^LUX_SCALE
TSL2561_PARAM_M1C = 0x01ad # 0.0262 * 2^LUX_SCALE
TSL2561_PARAM_K2C = 0x0085 # 0.260 * 2^RATIO_SCALE
TSL2561_PARAM_B2C = 0x0228 # 0.0337 * 2^LUX_SCALE
TSL2561_PARAM_M2C = 0x02c1 # 0.0430 * 2^LUX_SCALE
TSL2561_PARAM_K3C = 0x00c8 # 0.390 * 2^RATIO_SCALE
TSL2561_PARAM_B3C = 0x0253 # 0.0363 * 2^LUX_SCALE
TSL2561_PARAM_M3C = 0x0363 # 0.0529 * 2^LUX_SCALE
TSL2561_PARAM_K4C = 0x010a # 0.520 * 2^RATIO_SCALE
TSL2561_PARAM_B4C = 0x0282 # 0.0392 * 2^LUX_SCALE
TSL2561_PARAM_M4C = 0x03df # 0.0605 * 2^LUX_SCALE
TSL2561_PARAM_K5C = 0x014d # 0.65 * 2^RATIO_SCALE
TSL2561_PARAM_B5C = 0x0177 # 0.0229 * 2^LUX_SCALE
TSL2561_PARAM_M5C = 0x01dd # 0.0291 * 2^LUX_SCALE
TSL2561_PARAM_K6C = 0x019a # 0.80 * 2^RATIO_SCALE
TSL2561_PARAM_B6C = 0x0101 # 0.0157 * 2^LUX_SCALE
TSL2561_PARAM_M6C = 0x0127 # 0.0180 * 2^LUX_SCALE
TSL2561_PARAM_K7C = 0x029a # 1.3 * 2^RATIO_SCALE
TSL2561_PARAM_B7C = 0x0037 # 0.00338 * 2^LUX_SCALE
TSL2561_PARAM_M7C = 0x002b # 0.00260 * 2^LUX_SCALE
TSL2561_PARAM_K8C = 0x029a # 1.3 * 2^RATIO_SCALE
TSL2561_PARAM_B8C = 0x0000 # 0.000 * 2^LUX_SCALE
TSL2561_PARAM_M8C = 0x0000 # 0.000 * 2^LUX_SCALE


class TSL2561(object):
	def __init__(self, address=TSL2561_Address, i2c=None, **kwargs):
	    # Setup I2C interface for the device.
	    if i2c is None:
        	    import Adafruit_GPIO.I2C as I2C
	            i2c = I2C
		    self._device = i2c.get_i2c_device(address, **kwargs)  

            self._logger        = logging.getLogger('TSL2561')

            self.debug          = False
            self._packageType   = 0         # 0=T package, 1=CS package
            self._gain          = 0         # current gain: 0=1x, 1=16x [dynamically selected]
            self._gain_m        = 1         # current gain, as multiplier
            self._timing        = 2         # current integration time: 0=13.7ms, 1=101ms, 2=402ms [dynamically selected]
            self._timing_ms     = 0         # current integration time, in ms
            self._channel0      = 0         # raw current value of visible+ir sensor
            self._channel1      = 0         # raw current value of ir sensor
            self._schannel0     = 0         # normalized current value of visible+ir sensor
            self._schannel1     = 0         # normalized current value of ir sensor

            #reset device
            self._reset()

	def _reset(self):
       	    self._powerUp()
       	    self._setTintAndGain()
       	    self.writeRegister(TSL2561_REG_Interrupt, 0x00)
       	    self._powerDown()

   	def readRegister(self,address):
       	    try:
       	        byteval = self._device.readU8(address)
       	        if (self.debug):
       	            print("TSL2561.readRegister: returned 0x%02X from reg 0x%02X" % (byteval, address))
       	        return byteval
       	    except IOError:
       	        print("TSL2561.readRegister: error reading byte from reg 0x%02X" % address)
       	        return -1


        def writeRegister(self,address, val):
            try:
                self._device.write8(address, val)
                if (self.debug):
                    print("TSL2561.writeRegister: wrote 0x%02X to reg 0x%02X" % (val, address))
            except IOError:
                print("TSL2561.writeRegister: error writing byte to reg 0x%02X" % address)
                return -1

        def _powerUp(self):
            self.writeRegister(TSL2561_REG_Control, 0x03)

        def _powerDown(self):
            self.writeRegister(TSL2561_REG_Control, 0x00)

        def _setTintAndGain(self):
            #global gain_m, timing_ms

            if self._gain == 0:
                self._gain_m = 1
            else:
                self._gain_m = 16

            if self._timing == 0:
                self._timing_ms = 13.7
            elif self._timing == 1:
                self._timing_ms = 101
            else:
                self._timing_ms = 402
            self.writeRegister(TSL2561_REG_Timing, self._timing | self._gain << 4)

        def _readLux(self):
            time.sleep(float(self._timing_ms + 1) / 1000)

            ch0_low  = self.readRegister( TSL2561_REG_Channel0L )
            ch0_high = self.readRegister( TSL2561_REG_Channel0H )
            ch1_low  = self.readRegister( TSL2561_REG_Channel1L )
            ch1_high = self.readRegister( TSL2561_REG_Channel1H )

            # global channel0, channel1
            self.channel0 = ( ch0_high<<8 ) | ch0_low
            self.channel1 = ( ch1_high<<8 ) | ch1_low

            if self.debug:
                print("TSL2561.readVisibleLux: channel 0 = %i, channel 1 = %i [gain=%ix, timing=%ims]" % (self._channel0, self._channel1, self._gain_m, self._timing_ms))

        def _readVisibleLux(self):
            #global timing, gain

            self._powerUp()
            self._readLux()

            if self._channel0 < 500 and self._timing == 0:
                self._timing = 1
                if self.debug:
                    print("TSL2561.readVisibleLux: too dark. Increasing integration time from 13.7ms to 101ms")
                self._setTintAndGain()
                self._readLux()

            if self._channel0 < 500 and self._timing == 1:
                self._timing = 2
                if self.debug:
                    print("TSL2561.readVisibleLux: too dark. Increasing integration time from 101ms to 402ms")
                self._setTintAndGain()
                self._readLux()

            if self._channel0 < 500 and self._timing == 2 and self._gain == 0:
                self._gain = 1
                if self.debug:
                    print("TSL2561.readVisibleLux: too dark. Setting high gain")
                self._setTintAndGain()
                self._readLux()

            if ( self._channel0 > 20000 or self._channel1 > 20000 ) and self._timing == 2 and self._gain == 1:
                self._gain = 0
                if self.debug:
                    print("TSL2561.readVisibleLux: enough light. Setting low gain")
                self._setTintAndGain()
                self._readLux()

            if ( self._channel0 > 20000 or self._channel1 > 20000) and self._timing == 2:
                self._timing = 1
                if self.debug:
                    print("TSL2561.readVisibleLux: enough light. Reducing integration time from 402ms to 101ms")
                self._setTintAndGain()
                self._readLux()

            if ( self._channel0 > 10000 or self._channel1 > 10000) and self._timing == 1:
                self._timing = 0
                if self.debug:
                    print("TSL2561.readVisibleLux: enough light. Reducing integration time from 101ms to 13.7ms")
                self._setTintAndGain()
                self._readLux()

            self._powerDown()

            if ( self._timing == 0 and (self._channel0 > 5000 or self._channel1 > 5000)) or ( self._timing == 1 and ( self._channel0 > 37000 or self._channel1 > 37000)) or ( self._timing == 2 and ( self._channel0 > 65000 or self._channel1 > 65000)):
                # overflow
                return -1

            return self.calculateLux(self._channel0, self._channel1)

        def calculateLux(self,ch0, ch1):
            chScale = 0
            if self._timing == 0:   # 13.7 msec
                chScale = TSL2561_PARAM_CHSCALE_TINT0
            elif self._timing == 1: # 101 msec
                chScale = TSL2561_PARAM_CHSCALE_TINT1;
            else:           # assume no scaling
                chScale = (1 << TSL2561_PARAM_CH_SCALE)

            if self._gain == 0:
                chScale = chScale << 4 # scale 1X to 16X

            # scale the channel values
            #global schannel0, schannel1
            self._schannel0 = (ch0 * chScale) >> TSL2561_PARAM_CH_SCALE
            self._schannel1 = (ch1 * chScale) >> TSL2561_PARAM_CH_SCALE

            ratio = 0
            if self._schannel0 != 0:
                ratio = ( self._schannel1 << (TSL2561_PARAM_RATIO_SCALE+1)) / self._schannel0
            ratio = (ratio + 1) >> 1

            if self._packageType == 0: # T package
                if ((ratio >= 0) and (ratio <= TSL2561_PARAM_K1T)):
                    b=TSL2561_PARAM_B1T; m=TSL2561_PARAM_M1T;
                elif (ratio <= TSL2561_PARAM_K2T):
                    b=TSL2561_PARAM_B2T; m=TSL2561_PARAM_M2T;
                elif (ratio <= TSL2561_PARAM_K3T):
                    b=TSL2561_PARAM_B3T; m=TSL2561_PARAM_M3T;
                elif (ratio <= TSL2561_PARAM_K4T):
                    b=TSL2561_PARAM_B4T; m=TSL2561_PARAM_M4T;
                elif (ratio <= TSL2561_PARAM_K5T):
                    b=TSL2561_PARAM_B5T; m=TSL2561_PARAM_M5T;
                elif (ratio <= TSL2561_PARAM_K6T):
                    b=TSL2561_PARAM_B6T; m=TSL2561_PARAM_M6T;
                elif (ratio <= TSL2561_PARAM_K7T):
                    b=TSL2561_PARAM_B7T; m=TSL2561_PARAM_M7T;
                elif (ratio > TSL2561_PARAM_K8T):
                    b=TSL2561_PARAM_B8T; m=TSL2561_PARAM_M8T;
            elif self._packageType == 1: # CS package
                if ((ratio >= 0) and (ratio <= TSL2561_PARAM_K1C)):
                    b=TSL2561_PARAM_B1C; m=TSL2561_PARAM_M1C;
                elif (ratio <= TSL2561_PARAM_K2C):
                    b=TSL2561_PARAM_B2C; m=TSL2561_PARAM_M2C;
                elif (ratio <= TSL2561_PARAM_K3C):
                    b=TSL2561_PARAM_B3C; m=TSL2561_PARAM_M3C;
                elif (ratio <= TSL2561_PARAM_K4C):
                    b=TSL2561_PARAM_B4C; m=TSL2561_PARAM_M4C;
                elif (ratio <= TSL2561_PARAM_K5C):
                    b=TSL2561_PARAM_B5C; m=TSL2561_PARAM_M5C;
                elif (ratio <= TSL2561_PARAM_K6C):
                    b=TSL2561_PARAM_B6C; m=TSL2561_PARAM_M6C;
                elif (ratio <= TSL2561_PARAM_K7C):
                    b=TSL2561_PARAM_B7C; m=TSL2561_PARAM_M7C;

            temp = (( self._schannel0*b ) - ( self._schannel1*m ))
            if temp < 0:
                temp = 0;
            temp += ( 1<< ( TSL2561_PARAM_LUX_SCALE - 1 ))
            # strip off fractional portion
            lux = temp >> TSL2561_PARAM_LUX_SCALE
            if self.debug:
                print( "TSL2561.calculateLux: %i" % lux )

            return lux
