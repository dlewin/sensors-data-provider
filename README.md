A project for reporting measurements from different sensors plugged in a board like RaspberryPi or BeagleBone. Its purpose is to provide these data to a distant listener.



![Project overall view](doc/images/data_sensors.png)

There are different examples you can use in the "examples" directory:
- simpletest.py : Just prints out measures froom your sensors. It can also help you to validate your installation
- dataprovider.py: Simpletest wich outputs values to network through TCP 
- all_sensors_pub.py : like the simpletest but using ZeroMQ 

Some words about message queues.
They are used through the ZeroMQ library for data exchanges.
This project will use the PUB/SUB paradigm for its robustness and reliability.

If you don't know what ZeromQ or message queues are don't worry : the implementation is really simple so you can handle the code easily.
In the hope that you'll enjoy Zeromq so you will want to get to know it better.

**Requirements**

The projects is based on the following sensors:

- Sunlight sensor based on the SI1145 
- Digital light sensor based on the TSL2561
- Temperature/pressure (altitude computed) sensor based on BMP280
- Dust sensor based on the PPD42NS

**Installation**

1 We need to install the Adafruit libraries:
- git clone https://github.com/adafruit/Adafruit_Python_GPIO.git
- cd Adafruit_Python_GPIO/
- sudo python setup.py install

Automatic install is recommanded. Anyway if like me using the example provided complain about missing IO ligrary, you need to manually install the libs. Just follow these instructions:

2 Install the BMP280 repository:
- git clone https://github.com/bastienwirtz/Adafruit_Python_BMP.git
- cd Adafruit_Python_BMP
-  sudo python setup.py install

3 Install the SI1145 repository:
- git clone https://github.com/THP-JOE/Python_SI1145.git
- cd Python_SI1145
- sudo python setup.py install

4 Install the TSL2561 repository:
- git clone https://github.com/cmaion/TSL2561.git
- cd TSL2561

5 Install the Dexter repository:
- git clone https://github.com/DexterInd/GrovePi.git
- cd  GrovePi/Software/Python
- sudo python setup.py install

You now have all the codes to test the sensors. 

6 You just need now to install ZeroMQ:
- sudo pip install pyzmq
*This can take a little while as the complete librarie is compiled*
- cd sensors-data-provider/example
- edit bmp280_pubserver.py @ line 10 :
  - change the X in 192.168.0.**X** to fit to your board's address

**Running the provided example**

You are now ready to start the examples:

There is the simplest example that just prints out the sensors measures from classes instanciations:

``` 
    david@Jessie ~/sensors-data-provider/example $ python simpletest.py 
    Lux: 0 [vis+ir=0, ir=0 @ gain=16x, timing=402.0ms]
    Temp: 23.19 Pressure 23.19 Altitude 23.19 sea level 23.19
    Vis: 263
    IR: 276
    uvIndex: 0.03
``` 

There is another simple example (that will evolve) based on ZeroMQ to exchange the
measures on the network through subscribing pattern:

- from the board start **python bmp280_pubserver.py**. This simple example is sending continusly temperature, pressure and altitude data over the network. 
- from another board, PC, Mac, whatever is connected to your network, copy *subscriber.py*
- think to also change the board's address line 5 with the same address you set previously
- if this is a computer also think to get ZeroMQ installed (http://zeromq.org/intro:get-the-software)
- from your python environement (IDLE, pycharm, SublimeText, etc) start the listener : python subscriber.py
- you should now see that the both items are talking over the network like this :

``` 
    david@Jessie ~/sensors-data-provider/example $ python subscriber.py 
    temperature 24.62
    pressure 1019.42
    altitude 4.83
``` 










