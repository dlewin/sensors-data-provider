#!/usr/bin/python

import time
import Adafruit_BMP.BMP280 as BMP280
import SI1145.SI1145 as SI1145

#imports for grove modules
import grovepi
import atexit

import zmq

#importing our classes
import sys
sys.path.insert(0,'../src') 
import tsl2561_class

#configuring sensors
press_alt_sensor = BMP280.BMP280()
lux_sensor = tsl2561_class.TSL2561()
uv_sensor =  SI1145.SI1145() 

vis = uv_sensor.readVisible()
IR = uv_sensor.readIR()
UV = uv_sensor.readUV()
uvIndex = UV / 100.0

IP_ADDRESS="192.168.0.7"
PORT="5000"

#configuring grove sensors
atexit.register(grovepi.dust_sensor_dis)
grovepi.dust_sensor_en()

#zmq configuration
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind( "tcp://"+ IP_ADDRESS+ ":"+ PORT )
print ( "Starting to publish sensors data from :"+ IP_ADDRESS+ ":"+ PORT )

while (True):
    lux_msg = "Lux: %i [vis+ir=%i, ir=%i @ gain=%ix, timing=%.1fms]" % (lux_sensor._readVisibleLux(), lux_sensor._channel0, lux_sensor._channel1, lux_sensor._gain_m, lux_sensor._timing_ms)
    temp_press_msg =  "Temp {0:0.2f} Press {0:0.2f} Alt {0:0.2f} sea {0:0.2f}".format( press_alt_sensor.read_temperature(),press_alt_sensor.read_pressure(),press_alt_sensor.read_altitude(), press_alt_sensor.read_sealevel_pressure() )
    uv_msg =  "Vis: " + (str(vis)) + "IR: " + (str(IR)) + "uvIndex: " + (str(uvIndex))
    [new_val,lowpulseoccupancy] = grovepi.dustSensorRead()
    if new_val:
        dust_msg = "Dust: " + lowpulseoccupancy
    else:
        dust_msg = "Dust = x"
    
    socket.send( lux_msg )
    socket.send( temp_press_msg )
    socket.send( uv_msg )
    socket.send( dust_msg )
    time.sleep(2)
