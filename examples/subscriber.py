import zmq

context = zmq.Context()
socket2 = context.socket(zmq.SUB)
socket2.connect("tcp://192.168.0.7:5000")
socket2.setsockopt_string(zmq.SUBSCRIBE, "Temp")
socket2.setsockopt_string(zmq.SUBSCRIBE, "Lux")
socket2.setsockopt_string(zmq.SUBSCRIBE, "Vis")
socket2.setsockopt_string(zmq.SUBSCRIBE, "Dust")

while True:
    print (socket2.recv())
