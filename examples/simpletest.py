"""
  This example is using the sensors through their respective class. 
  TODO : reduce prints with measures vars
"""
#importing our classes
import sys
sys.path.insert(0,'../src') 
import tsl2561_class

import time
import Adafruit_BMP.BMP280 as BMP280
import SI1145.SI1145 as SI1145

#imports for grove modules
import grovepi
import atexit

atexit.register(grovepi.dust_sensor_dis)

while (True):
  #Get the measures updated
    grovepi.dust_sensor_en()

    press_alt_sensor = BMP280.BMP280()
    lux_sensor = tsl2561_class.TSL2561()
    uv_sensor =  SI1145.SI1145() 

    vis = uv_sensor.readVisible()
    IR = uv_sensor.readIR()
    UV = uv_sensor.readUV()
    uvIndex = UV / 100.0

  # now transorm the data and print them
    print ("Lux: %i [vis+ir=%i, ir=%i @ gain=%ix, timing=%.1fms]" % (lux_sensor._readVisibleLux(), lux_sensor._channel0, lux_sensor._channel1, lux_sensor._gain_m, lux_sensor._timing_ms))
    print ( "Temp: {0:0.2f} Pressure {0:0.2f} Altitude {0:0.2f} sea level {0:0.2f}".format( press_alt_sensor.read_temperature(),press_alt_sensor.read_pressure(),press_alt_sensor.read_altitude(), press_alt_sensor.read_sealevel_pressure() ))
    print ("Vis: " + (str(vis)))
    print ("IR: " + (str(IR)))
    print ("uvIndex: " + (str(uvIndex)))
    [new_val,lowpulseoccupancy] = grovepi.dustSensorRead()
    if new_val:
        print("Dust:",lowpulseoccupancy)
    time.sleep(2)
